package com.agsft.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.agsft.Employee;
import com.agsft.EmployeeService;

@Controller
public class MainController {
	@Autowired
	EmployeeService employeeservice;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public void Demo() {

		System.out.println("WELCOME");
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> insert(@RequestBody Employee emp) {

		if (employeeservice.insert(emp) != 0) {
			return new ResponseEntity<String>("Added Successfully", HttpStatus.OK);
		}

		return new ResponseEntity<String>("Not successfull", HttpStatus.OK);

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> update(@RequestBody Employee emp) {
		if (employeeservice.update(emp) != 0) {
			return new ResponseEntity<String>("Updated Successfully", HttpStatus.OK);
		}

		return new ResponseEntity<String>("Not successfull", HttpStatus.OK);

	}

	@RequestMapping(value = "/get", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> GetEmployeeById(@RequestParam(value = "Id") int Id) {

		return new ResponseEntity(employeeservice.getEmployee(Id), HttpStatus.OK);

	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> DeleteEmployee(@RequestParam(value  = "Id") int Id) {

		if (employeeservice.delete(Id) != 0) {
			return new ResponseEntity<String>("deleted Successfully", HttpStatus.OK);
		}

		return new ResponseEntity<String>("Not successfull", HttpStatus.OK);

	}

	@RequestMapping(value = "/getAll", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List> DeleteEmployee() {

		return new ResponseEntity<List>(employeeservice.findAll(), HttpStatus.OK);

	}

}
