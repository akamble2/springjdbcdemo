package com.agsft;

public class Employee {

	private int Id;
	private String FirstName;
	private String LastName;
	private int Salary;
	public Employee(int id, String firstName, String lastName, int salary) {
		
		Id = id;
		FirstName = firstName;
		LastName = lastName;
		Salary = salary;
	}
	public Employee() {
		
	}
	public long getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public int getSalary() {
		return Salary;
	}
	public void setSalary(int salary) {
		Salary = salary;
	}
	
	
	
	
	
	
	
	
}
