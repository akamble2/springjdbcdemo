package com.agsft;

import java.util.List;

public interface EmployeeService {
	
	public int insert(Employee e);

	public int update(Employee emp);
	
	public Employee getEmployee(int l);

	public int delete(int id);
	
	public List<Employee> findAll();
	
	

}
