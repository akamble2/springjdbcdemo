package com.agsft;

import java.util.List;

public interface EmployeeDao {
	
	public int InsertEmployee(Employee e);
	
	public Employee getEmployee(int l);
	
	public int UpdateEmployee(Employee e);
	
	public List<Employee> FindAll();

	public int delete(int id);

}
