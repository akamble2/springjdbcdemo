package com.agsft.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agsft.Employee;
import com.agsft.EmployeeDao;
import com.agsft.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	EmployeeDao empDao;

	public int insert(Employee e) {
		return empDao.InsertEmployee(e);
	}

	public int update(Employee emp) {
		return empDao.UpdateEmployee(emp);
	}

	public Employee getEmployee(int id) {
		return empDao.getEmployee(id);
	}

	

	public int delete(int id) {
		return empDao.delete(id);
	}

	public List<Employee> findAll() {
		// TODO Auto-generated method stub
		return empDao.FindAll();
	}

	
	

	

}
