package com.agsft.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.agsft.Employee;
import com.agsft.EmployeeDao;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	JdbcTemplate jdbc;
	
	
	public int InsertEmployee(Employee e) {
		String sql="INSERT INTO Employee(First_name,Last_name,Salary) VALUES (1,?,?,?)";
		
		return jdbc.update(sql,e.getFirstName(),e.getLastName(),e.getSalary());
		
	}


	public Employee getEmployee(int id) {
		String sql="select * from Employee where Id=?";
		return jdbc.queryForObject(sql,new Object[] {id},BeanPropertyRowMapper.newInstance(Employee.class));
	}


	public int UpdateEmployee(Employee e) {
		String sql="Update Employee set First_name=?,Last_name=?,Salary=? where Id=?";
		
		return jdbc.update(sql,e.getFirstName(),e.getLastName(),e.getSalary(),e.getId());
		
	}


	public List<Employee> FindAll() {
		String sql="Select * from Employee";
		List<Employee> employees = new ArrayList();

        List<Map<String, Object>> rows = jdbc.queryForList(sql);

        for (Map row : rows) {
            Employee obj = new Employee();
            obj.setId((int) row.get("Id"));
            obj.setFirstName((String) row.get("First_name"));
            obj.setLastName((String) row.get("Last_name"));
            String salary=row.get("Salary").toString();
            System.out.println(salary);
            obj.setSalary((Integer.parseInt(salary)));
            
            employees.add(obj);
        }

        return employees;
		
	}


	public int delete(int id) {
		String sql="DELETE from Employee where Id=?";
		
		return jdbc.update(sql,id);
		
	}


	
	

	

	

}
