package com.agsft.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
@Configuration
public class DataSourceConfig {

	@Bean
	DataSource dataSource() {
		DriverManagerDataSource datasource= new DriverManagerDataSource();
		datasource.setDriverClassName("com.mysql.jdbc.Driver");
		datasource.setUrl("jdbc:mysql://localhost:3306/Spring_JDBC");
		datasource.setUsername("akki");
		datasource.setPassword("Akshay@1997");
		return datasource;
	}
	
	@Bean
	@Autowired
	public JdbcTemplate getJdbcTemplate(DataSource datasource)
	{
		return new JdbcTemplate(datasource);
	}
	
}
