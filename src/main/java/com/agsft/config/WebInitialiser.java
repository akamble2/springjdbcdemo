package com.agsft.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class WebInitialiser implements WebApplicationInitializer {

	public void onStartup(ServletContext servletcontext) throws ServletException {
		AnnotationConfigWebApplicationContext crv= new AnnotationConfigWebApplicationContext();
		crv.register(WebConfig.class);
		crv.setServletContext(servletcontext);
		
		DispatcherServlet ds = new DispatcherServlet(crv);
		ServletRegistration.Dynamic servlet=servletcontext.addServlet("ds", ds);
		servlet.setLoadOnStartup(1);
		servlet.addMapping("/");

	}

}
